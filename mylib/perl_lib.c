#include "perl_lib.h"
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>

#define TELL_ERR_(type)                                                        \
  case type:                                                                   \
    return #type;

char const *fm_xfs_err_wrap(fm_xfs_err_t err) {
  switch (err) {
    // TELL_ERR_(FM_XFS_ERR_NONE)
    TELL_ERR_(FM_XFS_ERR_NOT_SUPPORTED)
    TELL_ERR_(FM_XFS_ERR_DEVICE_NOT_FOUND)
    TELL_ERR_(FM_XFS_ERR_DEVICE)
    TELL_ERR_(FM_XFS_ERR_OUT_DEVICE)
    TELL_ERR_(FM_XFS_ERR_MAGIC)
    TELL_ERR_(FM_XFS_ERR_FORMAT)
    TELL_ERR_(FM_XFS_ERR_FILENAME_NOT_FOUND)
    TELL_ERR_(FM_XFS_ERR_NOT_A_FILE)
    TELL_ERR_(FM_XFS_ERR_NOT_A_DIRECTORY)
  default:
    return "";
  }
}

void *fm_xfs_alloc(char const *device_path) {
  fm_xfs_t *fm = malloc(sizeof(fm_xfs_t));
  if (!fm)
    return NULL;
  if (fm_xfs_init(fm, device_path) != FM_XFS_ERR_NONE) {
    free(fm);
    return NULL;
  }
  return fm;
}

fm_xfs_err_t fm_xfs_dealloc(void *fm) {
  fm_xfs_err_t err = fm_xfs_free(fm);
  free(fm);
  return err;
}

static char pwd_buf[PATH_MAX];

char *fm_xfs_pwd_recurrent_(string_list_t *list, int is_first) {
  char *dest;
  if (!list) {
    dest = pwd_buf;
  } else {
    dest = fm_xfs_pwd_recurrent_(list->next, 0);
    strcpy(dest, list->data);
    dest += strlen(dest);
  }
  if (!is_first || !list)
    *dest++ = '/';
  return dest;
}

char const *fm_xfs_pwd(fm_xfs_t *fm) {
  *fm_xfs_pwd_recurrent_(fm->path, 1) = 0;
  return pwd_buf;
}

int fm_xfs_dir_entry_ls_callback(void *self, void *data) {
  fm_xfs_dir_entry_t *entry = data;
  switch (entry->ftype) {
  case XFS_DIR3_FT_DIR:
    fputs("d ", stdout);
    break;
  case XFS_DIR3_FT_REG_FILE:
    fputs("f ", stdout);
    break;
  default:
    fputs("? ", stdout);
    break;
  }
  puts(entry->name);
  return 1;
}
