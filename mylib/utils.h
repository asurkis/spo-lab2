#ifndef _UTILS_H_
#define _UTILS_H_

#include <stddef.h>

typedef int (*callback_t)(void *, void *);

typedef struct string_list {
  struct string_list *next;
  char data[1];
} string_list_t;

string_list_t *string_list_init(char const *str, size_t len);
void string_list_free(string_list_t *list);
void string_list_append(string_list_t **where, char const *str, size_t len);
void string_list_remove(string_list_t **where);

#endif
