#ifndef _PERL_LIB_H_
#define _PERL_LIB_H_

#include "devicelist.h"
#include "xfs.h"

char const *fm_xfs_err_wrap(fm_xfs_err_t err);
void *fm_xfs_alloc(char const *device_path);
fm_xfs_err_t fm_xfs_dealloc(void *fm);
char const *fm_xfs_pwd(fm_xfs_t *fm);
int fm_xfs_dir_entry_ls_callback(void *self, void *data);

#endif
