#!/usr/bin/perl
# use ExtUtils::testlib;
use strict;
use warnings;
# use FFI;

package DirEntry;

use FFI::Platypus::Record;
record_layout_1(
  'uint64' => 'inumber',
  'string(256)' => 'name',
  'uint8' => 'namelen',
  'uint8' => 'ftype'
);

package main;

use FFI::Platypus 1.00;
my $ffi = FFI::Platypus->new(api => 1);
$ffi->lib('mylib/build/libmylib.so');
$ffi->load_custom_type('::Enum', 'fm_xfs_err_t',
  'FM_XFS_ERR_NONE',
  'FM_XFS_ERR_NOT_SUPPORTED',
  'FM_XFS_ERR_DEVICE_NOT_FOUND',
  'FM_XFS_ERR_DEVICE',
  'FM_XFS_ERR_OUT_DEVICE',
  'FM_XFS_ERR_MAGIC',
  'FM_XFS_ERR_FORMAT',
  'FM_XFS_ERR_FILENAME_NOT_FOUND',
  'FM_XFS_ERR_NOT_A_FILE',
  'FM_XFS_ERR_NOT_A_DIRECTORY');
$ffi->type('record(DirEntry)' => 'DirEntry');
$ffi->type('(opaque,opaque)->int' => 'callback_t');
$ffi->attach(print_devices => ['int'] => 'void');
$ffi->attach(fm_xfs_ls => ['opaque', 'opaque', 'callback_t'] => 'fm_xfs_err_t');
$ffi->attach(fm_xfs_cd => ['opaque', 'string', 'size_t'] => 'fm_xfs_err_t');
$ffi->attach(fm_xfs_cp => ['opaque', 'string', 'string'] => 'fm_xfs_err_t');
$ffi->attach(fm_xfs_cat => ['opaque', 'string', 'size_t'] => 'fm_xfs_err_t');
$ffi->attach(fm_xfs_alloc => ['string'] => 'opaque');
$ffi->attach(fm_xfs_dealloc => ['opaque'] => 'fm_xfs_err_t');
$ffi->attach(fm_xfs_pwd => ['opaque'] => 'string');

if (@ARGV >= 1 && $ARGV[0] eq 'list') {
  if (@ARGV >= 2) {
    device_list($ARGV[1]);
  } else {
    device_list(-1);
  }
} elsif (@ARGV >= 2 && $ARGV[0] eq 'open') {
  start_ui($ARGV[1]);
} else {
  print_usage();
}

sub device_list {
  print_devices($_[0]);
  # FFI::print_devices($_[0]);
}

sub start_ui {
  my $fm = fm_xfs_alloc($_[0]);
  # my $fm = FFI::fm_xfs_alloc($_[0]);
  return until $fm;

  print fm_xfs_pwd($fm), ">";
  # print FFI::fm_xfs_pwd($fm), ">";
  LOOP:
  while (my $line = <STDIN>) {
    $line =~ s/\s+$//;
    my $result = 'FM_XFS_ERR_NONE';
    if ($line =~ /^ls/) {
      $result = call_ls($fm, $line);
    } elsif ($line =~ /^cd/) {
      $line =~ s/^cd\s*//;
      $result = call_cd($fm, $line);
    } elsif ($line =~ /^cp/) {
      $line =~ s/^cp\s*//;
      $result = call_cp($fm, $line);
    } elsif ($line =~ /^cat/) {
      $line =~ s/^cat\s*//;
      $result = call_cat($fm, $line);
    } elsif ($line =~ /^exit/) {
      last LOOP
    } else {
      print "Unknown command '$line'\n"
    }
    print $result, "\n" if $result ne 'FM_XFS_ERR_NONE';
    print fm_xfs_pwd($fm), ">";
    # print FFI::fm_xfs_pwd($fm), ">";
  }

  fm_xfs_dealloc($fm);
  # FFI::fm_xfs_dealloc($fm);
}

sub call_ls {
  return fm_xfs_ls($_[0], 0, $ffi->closure(sub {
    my $rec = $ffi->cast('opaque' => 'DirEntry *', $_[1]);
    if ($rec->ftype == 1) {
      print "f ";
    } elsif ($rec->ftype == 2) {
      print "d ";
    } else {
      print "? ";
    }
    print substr($rec->name, 0, $rec->namelen), "\n";
  }));
  my $result = FFI::fm_xfs_ls($_[0]);
}

sub call_cd {
  $_[1] =~ /^\s*(\S+)\s*$/;
  return fm_xfs_cd($_[0], $1, length $1);
  # my $result = FFI::fm_xfs_cd($_[0], $1, length $1);
}

sub call_cp {
  $_[1] =~ /^\s*(\S+)\s+(\S+)\s*$/;
  return fm_xfs_cp($_[0], $1, $2);
  # my $result = FFI::fm_xfs_cp($_[0], $1, $2);
}

sub call_cat {
  $_[1] =~ /^\s*(\S+)\s*$/;
  return fm_xfs_cat($_[0], $1, length $1);
  # my $result = FFI::fm_xfs_cat($_[0], $1, length $1);
}

sub print_usage {
  print "Usage:
\t./main.pl list [header interval]
\t\t-- list mounted devices
\t\t   header interval values:
\t\t     <0 -- disabled (default)
\t\t      0 -- display only the first header
\t\t      N -- display header for each Nth line\n
\t./main.pl open <device>
\t\t-- open an xfs filesystem\n";
}
